/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tablero.modelo;

/**
 *
 * @author cloaiza
 */
public class Coordenada {
    private byte columna;
    private byte fila;
    private boolean estado;

    public Coordenada() {
    }

    public Coordenada( byte fila, byte columna, boolean estado) {
        this.columna = columna;
        this.fila = fila;
        this.estado = estado;
    }

    
    
    public byte getColumna() {
        return columna;
    }

    public void setColumna(byte columna) {
        this.columna = columna;
    }

    public byte getFila() {
        return fila;
    }

    public void setFila(byte fila) {
        this.fila = fila;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }
    
    
    
    
}
