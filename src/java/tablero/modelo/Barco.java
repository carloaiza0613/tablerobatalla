/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tablero.modelo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author carloaiza
 */
public class Barco implements Serializable{
    private String nombre;
    private List<Coordenada> coordenadas;

    public Barco(String nombre) {
        this.nombre = nombre;
        coordenadas= new ArrayList<>();
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<Coordenada> getCoordenadas() {
        return coordenadas;
    }

    public void setCoordenadas(List<Coordenada> coordenadas) {
        this.coordenadas = coordenadas;
    }
    
    public boolean validarCoordenada(int fila, int columna)
    {
        for(Coordenada coord: coordenadas)
        {
            if(coord.getFila()==fila && coord.getColumna()==columna)
            {
                return true;
            }
        }
        return false;
    }
    
    
}
