/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tablero.controlador;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import tablero.controlador.util.JsfUtil;
import tablero.modelo.Barco;
import tablero.modelo.Coordenada;
import tablero.modelo.Disparo;

/**
 *
 * @author cloaiza
 */
@Named(value = "controladorTablero")
@ApplicationScoped
public class ControladorTablero {

   
    
    private int tamTablero=10;
    
    private List<Disparo> disparosJuga1=new ArrayList<>();
    
    
    private List<Barco> listadoBarcos= new ArrayList<>();

    public List<Barco> getListadoBarcos() {
        return listadoBarcos;
    }

    public void setListadoBarcos(List<Barco> listadoBarcos) {
        this.listadoBarcos = listadoBarcos;
    }

    
    
    public int getTamTablero() {
        return tamTablero;
    }

    public void setTamTablero(int tamTablero) {
        this.tamTablero = tamTablero;
    }
    
    
    
    
    /**
     * Creates a new instance of ControladorTablero
     */    
    
    /**
     * Creates a new instance of ControladorTablero
     */
    public ControladorTablero() {
    }
 
    @PostConstruct
    public void iniciar()
    {
        Barco lancha= new Barco("Lancha");
        lancha.getCoordenadas().add(new Coordenada((byte)4, (byte)5, true));
        lancha.getCoordenadas().add(new Coordenada((byte)4, (byte)6, true));
        
        listadoBarcos.add(lancha);
        
        
         Barco acorazado= new Barco("Acorazado");
        acorazado.getCoordenadas().add(new Coordenada((byte)5, (byte)9, true));
        acorazado.getCoordenadas().add(new Coordenada((byte)6, (byte)9, true));
         acorazado.getCoordenadas().add(new Coordenada((byte)7, (byte)9, true));
        acorazado.getCoordenadas().add(new Coordenada((byte)8, (byte)9, true));
        
        
        listadoBarcos.add(acorazado);
    }
    
    public String pintarBarcos(int fila, int columna)
    {
        for(Barco miBarquito:listadoBarcos)
        {
            if(miBarquito.validarCoordenada(fila, columna))
            {
               if(miBarquito.getNombre().compareTo("Lancha")==0)
               {
                return "width: 80px; heigth: 80px; background-color: green;";
               }
               else if(miBarquito.getNombre().compareTo("Acorazado")==0)
               {
                   return "width: 80px; heigth: 80px; background-color: blue;";
               }
                   
            }
        }
        return "width: 80px; heigth: 80px;";
    }
    
    
    public boolean validarCoordenadaOcupada(int fila, int columna)
    {
        if(fila==2 && columna ==2)
        {
            return true;
        }    
        return false;
    }        
    
    
    public void disparar(int fila, int columna)
    {
        JsfUtil.addSuccessMessage("Has disparado en la fila: "+fila + " columna: "+columna);
    }
    
    public String obtenerEstilo(int fila, int columna)
    {
        if(fila %2 ==0 && columna %2 ==0)
        {
            return "width: 80px; height: 80px; background-color: blue";
        }    
        
        return "width: 80px; height: 80px;";
    }   
    
    
    
    public boolean verififcarDisparo(int fila, int columna)
    {
        for(Disparo disp: disparosJuga1)
        {
            if(disp.getFila()==fila && disp.getColumna()==columna)
            {
                return true;
            }    
        }        
        return false;
    }
    
    
    public void dispararJug1(int fila, int columna)
    {
        disparosJuga1.add(new Disparo(fila, columna));
    }
    
    
}
